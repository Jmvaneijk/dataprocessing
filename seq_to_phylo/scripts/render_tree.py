#!/usr/bin/env python3

"""
Using the Phylo and matplotlib packages, the created newick file is loaded and rendered to a .png
"""

__author__ = "Jamie van Eijk"
__version__ = "1.0.0"

from Bio import Phylo
from matplotlib import pyplot as plt

# Getting a tree object from the newick
tree = Phylo.read(snakemake.input[0], "newick")
tree.ladderize()

# Setting the image specs
fig = plt.figure(figsize=(10, 20), dpi=100)
axes = fig.add_subplot(1, 1, 1)

# Drawing the tree
Phylo.draw(tree, axes=axes, do_show=False)

# Save as PNG
plt.savefig(snakemake.output[0])