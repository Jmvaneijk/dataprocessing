#!/bin/bash
source ../venv/bin/activate
snakemake --snakefile Snakefile --cores 2
deactivate