# From Sequence to Phylogenetic Tree #

## Introduction ##

What if you only have a single, possibly unknown, sequence and you would like to make a phylogenetic tree to 
investigate the different relationships or identify the sequence? Then you have come to the right place!  

Using this Snakemake pipeline you can submit multiple fasta, pdb and/or genbank files at the same time and get images
of phylogenetic trees based on the input files as output.

This is done by BLASTing the sequence against the NCBI protein database (-evalue 0.001 -max_target_seqs 40), 
saving the output as a multi-fasta, converting this to a Multiple Sequence Alignment (MSA) 
and lastly creating a newick based tree as an image. 


## Installations -- Linux Only ##

Before you are able to properly use this pipeline, a couple of preparatory steps are required.


### Commandline Dependencies ###

The pipeline utilizes a couple of commandline tools which can be installed as follows:

#### Blast ####
```
# For most recent version
[sudo] apt-get install ncbi-blast+

# For 2.8.1 (since 2.9 version contain a bug with -remote)
[sudo] apt-get install ncbi-blast+=2.8.1
```

The [ncbi-blast+ 2.8.1](https://www.ncbi.nlm.nih.gov/books/NBK279690/) package contains the blastp option,
which is used to blast the sequence againt the NCBI protein database.

#### Muscle #### 
```
# Recommended to use the most recent version
[sudo] apt-get install muscle
```

[MUSCLE v3.8.1551](https://www.ebi.ac.uk/Tools/msa/muscle/) is used to create the MSA from a created multi-fasta file.

#### FastTree ####
```
# Recommended to use the most recent version
[sudo] apt-get install fasttree
```

Using [FastTree 2.1.11](http://www.microbesonline.org/fasttree/) a Newick (distance) file can be made from a MSA


### Setting up python virtual environment ###

With the commandline dependencies installed, it is time to prepare a python virtual environment from which
the Snakemake commands can be called.

#### Creating a virtual environment ####

Before installing the package into the virtual environment, a virtual environment should be created.
this can be done with the virtualenv package and can be install as follows.

```
# Can also be done with pip, but 3 specifies python3.
pip3 install virtualenv
```

With virtualenv installed the environment can be created like this.

```
# I'm calling the environment 'phylo_venv', but you can name it whatever you want.
virtualenv -p /usr/bin/python3 phylo_venv
```

After the phylo_venv is created you can load it and install the packages within \
[*IMPORTANT*] The environment should be loaded every time a new terminal is started for the pipeline to work!

```
source phylo_venv/bin/activate
```

The virtual environment can simply be disabled by typing.

```
deactivate
```

#### Installing python packages ####

The most important package to install is the [snakemake](https://snakemake.readthedocs.io/en/stable/) package. 
This package is the core of the pipeline, and is used to call and run the different rules. 

(As stated when installing virtualenv, I use pip3 to specify python3, but just pip can also work in some cases.)

```
pip3 install snakemake
```

Another important package is [Bio-Python](https://biopython.org/). Within this pipeline Bio-Python is used 
to fetch the fasta sequences from NCBI using the accession number acquired through BLAST,
convert the PDB and Genbank files to fasta and to draw the phylogenetic tree from the created newick file.

```
pip3 install biopython
```

Finally we need the [matplotlib](https://matplotlib.org/) package to save the drawn phylogenetic tree as an image.

```
pip3 install matplotlib
```

## Getting Started ##

With our virtual environment set up and the commandline tools installed, it is almost time to run the pipeline.
Because you are (probably) still in the root of the directory it is time to move to the seq_to_phylo folder.
Within this folder is the Snakefile, which we will visit shortly, and the config.yaml file. 

### Config ###
Before you can run the pipeline, a couple fields within the config file should be filled. 
The first field is the sequence_files. Within this field you need to specify the names of each fasta, pdb or genbank 
file you would like to use as input.

The next field is the workdir. This is the location where the pipeline can find your inputs and save the outputs. 
Within this workdir you should also place the above mention fasta, pdb or genbank files in fasta, pdb and genbank
folders, otherwise the pipeline cannot find these.

An example of a fully finished config file can be seen in the following example:

```
# Names of the input files, no extension required
sequence_files:
  # Fasta files
  Hemoglobin_Beta: Hemoglobin_Beta
  Histone_H3_2: Histon_H3_2
  Ribosome: Ribosome
  # PDB files
  dna_polymerase: dna_polymerase
  # Genbank
  mhc1: mhc1

# Set the working directory for your outputs
workdir: ../Commons/data/seq_to_phylo/
```

### Executing the Pipeline ###

After everything has been prepared, you can call the snakemake file within the seq_to_phylo as seen below,
and the result will be images of trees found in the results folder.

```
# Depending on the strength of the computer, you can use more cores for faster results.
snakemake --snakefile Snakefile --cores 1
```

The pipeline can also be visualized as a DAG image with the following command.

```
snakemake --forceall --dag | dot -Tpng > dag.png
```

## Contact ##

If you have question, suggestions or issues, feel free to contact me at:
ja.m.van.eijk@st.hanze.nl

And I will try to get back to you!