#!/usr/bin/env python3

"""
replaces the spaces in the headers with _ to create a larger, easier to understand yet unique name
"""

__author__ = "Jamie van Eijk"
__version__ = "1.0.0"


with open(snakemake.input[0], "r") as file:
    with open(snakemake.output[0], "w") as out:
        for line in file:
            if line.startswith(">"):
                line = line.split()
                line = ">" + "_".join(line) + "\n"
            out.write(line)
