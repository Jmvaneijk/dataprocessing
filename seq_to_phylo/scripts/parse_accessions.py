#!/usr/bin/env python3

"""
Because the BLAST output contains 2 columns, one for the query accession
and the other with the target accession number, this small script writes
the unique accession numbers to a new file
"""

__author__ = "Jamie van Eijk"
__version__ = "1.0.0"

accessions = []

# Collect all the accession numbers
with open(snakemake.input[0], "r") as file:
    for item in file.read().split("\t"):
        accession = item.split("\n")[0]
        accession = accession.split(".")[0]

        # Checking if duplicate accession not in list. If not, add to list
        if accession not in accessions:
            accessions.append(accession)

# Writing the non duplicate accessions
with open(snakemake.output[0], "w") as out:
    for accession in accessions:
        out.write(accession + "\n")
