#!/usr/bin/env python3

"""
Converts a PDB file to a fasta file using SeqIQ from biopython
"""

__author__ = "Jamie van Eijk"
__version__ = "1.0.0"

from Bio import SeqIO

with open(snakemake.input[0], 'r') as pdb_file:
    with open(snakemake.output[0], "w") as out:
        for record in SeqIO.parse(pdb_file, 'pdb-atom'):
            out.write('>' + record.id + "\n")
            out.write(str(record.seq))
