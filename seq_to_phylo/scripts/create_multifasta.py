#!/usr/bin/env python3

"""
Fetches the fasta sequences for the collected BLAST accession numbers
"""

__author__ = "Jamie van Eijk"
__version__ = "1.0.0"

import os
from Bio import Entrez

ids = []

# Filling a list with IDS
with open(snakemake.input[0], "r") as file:
    for line in file:
        line = line.split("\n")
        ids.append(line[0])

# Setting an Email to use NCBI/Entrez
Entrez.email = "ja.m.van.eijk@st.hanze.nl"

# Fetching the fasta for each accession in the list and adding them to the multifasta
if not os.path.isfile(snakemake.output[0]):
    net_handle = Entrez.efetch(
        db="protein", id=ids, rettype="fasta", retmode="text"
    )
    out_handle = open(snakemake.output[0], "w")
    out_handle.write(net_handle.read())
    out_handle.close()
    net_handle.close()
